<?php
session_start();
include "config.php";
?>
<!DOCTYPE html>    
<html>    
<head>  
<style>
    .common_class{display:none}
    label
    {
        text-align: left;
        font-size: 20px;
        color: #0080ff;
        font-family: monospace;
    }
</style>
<title>Edit Profile</title>    
<link rel="stylesheet" href="editprofile.css">   
</head>    
<body> 
<br>
<br>
<div class="header_div"> 
    <div id="logout_btn">
    <button type="button"  id="btn_logout"><a href="logout.php" title="Logout">Logout</a></button>
    </div>
    <div style="color: green; font-size: 25px; text-align: right;">
        <?php
        $first_word=$_SESSION["full_name"];
        $first_letter=$first_word[0];
        echo "<h1>";
        echo $first_letter;
        echo "</h1>";
        ?>
    </div> 
</div>
<div class="side_div" >
    <a href="registration1.php">Calculate Tax</a>  
    &nbsp;    &nbsp; &nbsp;  &nbsp;&nbsp;
    <a href="edit_profile1.php">Edit Profile</a>   
</div>
<h2 align="center"><u>Edit Profile</u></h2><br>    
<div class="edit_form" align="center">    
<form id="edit_form" method="POST" action="edit_profile1.php">  
    <label><b>Full Name : &nbsp;     &nbsp;    &nbsp;     &nbsp;    &nbsp;   &nbsp;     &nbsp;    &nbsp; &nbsp;     &nbsp;    
    </label>    
    <input type="text" class="input" name="full_name" id="full_name"  value= " <?php echo $_SESSION["full_name"];?>">
    <br><br>  
    <label><b>Password :  &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp;   &nbsp;     &nbsp;    &nbsp; &nbsp;  </b>    
    </label>    
    <input type="Password" class="input" name="password" id="password" placeholder="Password">    
    <br><br> 
    <div class='common_class class_for_valpwd1' >
    &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp; 
        Password should be atleast 8 characters 
    </div> 
    <label><b>Conform Password :  &nbsp;     &nbsp; </b>     
    </label>    
    <input type="Password" class="input" name="con_password" id="con_password" placeholder="Conform Password">   
    <br><br> 
    <div class='common_class class_for_valpwd' >
          &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp; 
          Password should be atleast 8 characters 
    </div>
    <div class='common_class class_for_ConfmPwd' >
          &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp; 
        Password and confirmed password should be same
    </div>      
    <br><br> 
    <input type="submit" name="submit_btn" id="submit_btn" value="Submit">       
    <br><br>      
</form>  
</div> 
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    $(document).on("click", "#submit_btn", function(){
    var passwd = $("#con_password").val();
    if(passwd.length <=6) 
    {
        $(".class_for_valpwd").show();
    }
    else
    {
        $(".class_for_valpwd").hide();
    }
    var passwd = $("#password").val();
    if(passwd.length <=6) 
    {
        $(".class_for_valpwd1").show();
    }
    else
    {
        $(".class_for_valpwd1").hide();
    }
    var psw1 = $("#password").val();
    var psw2 = $("#con_password").val();
    is_success = true;
    if(psw1 == '') 
    {
        $("#password").addClass('error_class');
        is_success = false;
    }
    else
    {
        $("#password").removeClass('error_class');
    }
          
    if(psw2 == '') 
    {
        $("#con_password").addClass('error_class');
        is_success = false;
    }
    else
    {
        $("#con_password").removeClass('error_class');
    }
        
    if(!is_success) 
    {
        return false;
    } 
    var firstpwd = $("#password").val();
    var conpwd = $("#con_password").val();
    if(firstpwd === conpwd)  
    {
        $(".class_for_ConfmPwd").hide();
        return true;
    }
    else
    {
        $(".class_for_ConfmPwd").show();
        return false;
    }
       
    });
</script> 
<?php
if(isset($_POST['submit_btn']))
{
    $pwd = mysqli_real_escape_string($con,$_POST['password']);
    $conpwd = mysqli_real_escape_string($con,$_POST['con_password']);
    $newpassword = md5($pwd);
    $userid = @$_SESSION['user_id'];
    if ($pwd != "" && $conpwd != "")
    {
        $sql_query = " UPDATE users SET password='" .$newpassword. "' WHERE user_id = '" .$userid. "' ";
        $result = mysqli_query($con,$sql_query);
        if($result== true)
        {
            echo '<script>alert("Password Updated Successfully..")</script>';
        }
    }
}
?>
</body>    
</html>   

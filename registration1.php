<?php
session_start();
?>
<!DOCTYPE html>    
<html>    
<head>  
<style>
    .class_for_val_income{display:none}
    .tax_calc{display:none}
    #submit_reg
    {
        background-color: #00CED1;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 30px;
        padding-right: 30px;
        margin-bottom: 20px;
        color: white;
        border: none;
    }
    label
    {
        text-align: right;
        font-size: 20px;
        color: #0080ff;
        font-family: monospace;
    }
</style>
<title>Registration Form</title>    
<link rel="stylesheet" href="editprofile.css">       
</head> 
<body>
<br>
<br>
<div class="header_div"> 
    <div id="logout_btn">
        <button type="button"  id="btn_logout"><a href="logout.php" title="Logout">Logout</a></button>
    </div>
    <div style="color: green; font-size: 25px; text-align: right;">
    <?php
    $first_word=$_SESSION["full_name"];
    $first_letter=$first_word[0];
    echo "<h1>";
    echo $first_letter;
    echo "</h1>";
    ?>
    </div> 
</div>
<div class="side_div" >
    <a href="registration1.php">Calculate Tax</a>  
    &nbsp;    &nbsp; &nbsp;  &nbsp;&nbsp;
    <a href="edit_profile1.php">Edit Profile</a>   
</div>
<h2 align="center"><u>Calculate Tax</u></h2><br>     
<div class="tax_calc">
    <label>Your Calculated Yearly Tax...</label>
</div>
<div class="registration_form">    
    <form id="registration_form" method="POST" action="registration1.php">  
    <label><b>Full Name : &nbsp;     &nbsp;  &nbsp;  &nbsp; &nbsp; </b>
    </label>    
    <input type="text" class="input" name="fullname" id="fullname" value= " <?php echo $_SESSION["full_name"];?>">  
    <br><br>  
    <label><b>Age :  &nbsp;     &nbsp;    &nbsp;   &nbsp;     &nbsp;    &nbsp; &nbsp;     &nbsp;    &nbsp; &nbsp; </b> 
    </label>    
    <input type="number" class="input" name="age" id="age" placeholder="Type Here">     
    <br><br> 
    <label><b>Annual Income :  &nbsp; </b>  
    <br> 
    </label>    
    <input type="text" class="input" name="annuel_income" id="annuel_income" placeholder="Type Here">    
    <br><br> 
    <div class='class_for_val_income' style="color:red;" >
    &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp;  &nbsp;     &nbsp;    &nbsp; 
        Enter a Valid Salary Amount 
    </div> 
    <br><br> 
    &nbsp;   &nbsp;     &nbsp;    &nbsp; &nbsp;     &nbsp;    &nbsp; &nbsp;  &nbsp;  
    <input type="Submit" name="submit_reg" id="submit_reg" value="Submit">      
    <br><br> 
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
$(document).on("click", "#submit_reg", function()
{
    var name1 = $("#fullname").val();
    var anninc = $("#annuel_income").val();
    var age1 = $("#age").val();
    is_success = true;
    if(name1 == '') 
    {
        $("#fullname").addClass('error_class');
        is_success = false;
    }
    else
    {
        $("#fullname").removeClass('error_class');
    }
    if(anninc == '') 
    {
        $("#annuel_income").addClass('error_class');
        is_success = false;
    }
    else
    {
        $("#annuel_income").removeClass('error_class');
    }
    if(age1 == '') 
    {
        $("#age").addClass('error_class');
        is_success = false;
    }
    else
    {
        $("#age").removeClass('error_class');
    }
    if(!is_success) 
    {
        return false;
    }
    if(isNaN(anninc))
    {
        $(".class_for_val_income").show();
        return false;
    }
    else
    {
        $(".class_for_val_income").hide();
        return true;
    }
});
</script> 
<?php
if(isset($_POST['submit_reg']))
{
    include "config.php";
    $age = mysqli_real_escape_string($con,$_POST['age']);
    $salary = mysqli_real_escape_string($con,$_POST['annuel_income']);
    Global $total_tax ;
    Global $grand_total_tax1 ;
    $grand_total_tax1=0;
    $total_tax=0;
    $sql ="SELECT * FROM `admin_income` WHERE age<{$age} and age in (SELECT max(age) FROM `admin_income` WHERE age<{$age} order by `age` desc) order by `age` desc";
    $result = mysqli_query($con, $sql);
    while($row = mysqli_fetch_assoc($result))
    {
        $start_income = $row['start_incom'] ;
        $end_income = $row['end_incom'] ;
        $percentage = $row['percentage'] ;
        if ($salary>5000000) 
        {
            echo" Annuel income should be in range 25,00000-5000000 ";
            break;
        }
          
        elseif($salary >= $start_income && $salary >$end_income)
        {
            $diff = $end_income - $start_income;
            $tax_in_this_slab = ($percentage/100)*$diff;
            $total_tax = $total_tax + $tax_in_this_slab;
        }
        elseif($salary >= $start_income && $salary<= $end_income)
        {
            $diff1 = $salary - $start_income;
            $tax_in_this_slab1 = ($percentage/100)*$diff1;
            $grand_total_tax1 = $total_tax + $tax_in_this_slab1;
        } 
    }      
?>
<div style="color: red;  background-color: #fed8b1;  border-color: gray; border-style: dashed; width: 40%;   padding:35px 15px 15px 0; font-size: 25px; text-align: right;">
<?php
echo "Your Calculated Yearly Tax";
echo "<br>";
echo "INR ".$grand_total_tax1;
echo "</div>";
}
?>
</body>    
</html>   
